package com.teosoft.rnd.enums;

import java.lang.reflect.Type;

/**
 * Another idea from here: http://stackoverflow.com/questions/9662170/java-override-valueof-and-tostring-in-enum
 */
public enum DispatchEnumEx {

	Start(String.class, new Handler() {

		public String handle() {
			// TODO Auto-generated method stub
			return null;
		}}),
	Stop(Integer.class, new Handler() {

		public String handle() {
			// TODO Auto-generated method stub
			return null;
		}});
	
	private final Handler handler;
	private Type type;
	
	DispatchEnumEx(Type type, Handler handler) {
		this.handler = handler;
		this.type = type;
	}
	
	public static Handler getHandler(Type type) {
		return getDispatch(type).handler;
	}
	
	public static DispatchEnumEx getDispatch(Type type) {
		for(DispatchEnumEx v : values()) {
			if(v.type.equals(type)) {
				return v;
			}
		}
		throw new IllegalArgumentException();
	}
}
