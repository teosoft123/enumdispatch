package com.teosoft.rnd.enums;

/**
 * @author otsvinev The idea comes from here:
 *         https://schneide.wordpress.com/2010/12/13/avoid-switch-use-enum/
 */
public enum DispatchEnum {
	A {
		@Override
		public Handler getHandler() {
			return new Handler() {

				public String handle() {
					return "A";
				}
			};
		}
	},

	B {
		@Override
		public Handler getHandler() {
			return new Handler() {

				public String handle() {
					return "B";
				}
			};
		}
	};

	public abstract Handler getHandler();
	
	public static DispatchEnum forKey(int code) {
		switch (code) {
		case 0:
			return A;
		case 1:
			return B;
		default:
			throw new IllegalArgumentException("Invalid code");
		}
	}

}
