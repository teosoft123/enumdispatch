package com.teosoft.rnd.enums;

import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.CoreMatchers.equalTo;


public class DispatchEnumExTest {

	@Test
	public void mustDispatch() {
		assertThat(DispatchEnumEx.Start, equalTo(DispatchEnumEx.getDispatch(String.class)));
		assertThat(DispatchEnumEx.Stop, equalTo(DispatchEnumEx.getDispatch(Integer.class)));
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void mustCatchWrongTypeToDispatch() {
		assertThat(DispatchEnumEx.Start, equalTo(DispatchEnumEx.getDispatch(Enum.class)));
		// To Be Continued
	}
	
}
