package com.teosoft.rnd.enums;

import org.junit.Test;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.CoreMatchers.equalTo;

public class DispatchEnumTest {

	@Test
	public void mustDispatch() {
		assertThat("A", equalTo(DispatchEnum.valueOf("A").getHandler().handle()));
		assertThat("B", equalTo(DispatchEnum.valueOf("B").getHandler().handle()));
	}
	
	@Test
	public void mustDispatchByCode() {
		assertThat("A", equalTo(DispatchEnum.forKey(0).getHandler().handle()));
		assertThat("B", equalTo(DispatchEnum.forKey(1).getHandler().handle()));
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void mustThrowForWrondCode() {
		assertThat("A", equalTo(DispatchEnum.forKey(666).getHandler().handle()));
	}

}
